module.exports = function(RED) {
    function RoboMowApiNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg, send, done){
            // For maximum backwards compatibility, check that send exists.
            // If this node is installed in Node-RED 0.x, it will need to
            // fallback to using `node.send`
            send = node.send || function() { node.send.apply(node,arguments) }
            
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText;
                }
            };            
            
            // get accesstoken
            var headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Content-Type': 'application/x-www-form-urlencoded',
                "Origin": 'https://myrobomow.robomow.com'
            };
            if (n.headers) {
                headers = Object.assign(
                    {},
                    headers,
                    n.headers.reduce(function(obj, item) {
                        obj[item.name] = item.value
                        return obj
                    }, {})
                );
            }
            msg['request-headers'] = headers;

            var formdata = n.formdata.reduce(function(obj, item) {
                obj[item.name] = item.value
                return obj
            }, {})

            var options = {
                method: 'POST',
                url: 'https://myrobomow.robomow.com/api/account/authenticate',
                headers: headers,
                formData: ''
            };

            var thisReq = request(options, function(err, resp, body) {
                // remove sending status
                node.status({});

                //Handle error
                if (err || !resp) {
                    // node.error(RED._("httpSendMultipart.errors.no-url"), msg);
                    var statusText = "Unexpected error";
                    if (err) {
                        statusText = err;
                    } else if (!resp) {
                        statusText = "No response object";
                    }
                    node.status({
                        fill: "red",
                        shape: "ring",
                        text: statusText
                    });
                }
                msg.payload = body;
                msg.statusCode = resp.statusCode || resp.status;
                msg['http-send-multipart-headers'] = resp.headers;
                msg['http-send-multipart-options'] = options;

                if (node.ret !== "bin") {
                    msg.payload = body.toString('utf8'); // txt

                    if (node.ret === "obj") {
                        try {
                            msg.payload = JSON.parse(body);
                        } catch (e) {
                            node.warn(RED._("httpSendMultipart.errors.json-error"));
                        }
                    }
                }
                send(msg);

                if (done) {
                    done();
                }
            });            
        });        
    }
    RED.nodes.registerType("robomow-api",RoboMowApiNode);
}